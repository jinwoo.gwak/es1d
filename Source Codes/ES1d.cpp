#include "define.h"
#include "ES1D.h"

void showParticlesPosition(particle **particles){
    std::cout << "\n";
    std::cout << std::right;
    std::cout << "---show Particle Positions---" << std::endl;
    for(int i=0; i<particle::getTotalNumPtl(); i++){
        std::cout 
        << std::defaultfloat << std::setw(5)  << particles[i]->getChargeRatio()
        << std::scientific   << std::setw(16) << particles[i]->getNormalizedPos() << std::endl;
    }
    std::cout << std::endl;
}

void showParticlesVelocity(particle **particles){
    std::cout << "\n";
    std::cout << std::right;
    std::cout << "---show Particle Velocity---" << std::endl;
    for(int i=0; i<particle::getTotalNumPtl(); i++){
        std::cout 
        << std::defaultfloat << std::setw(5)  << particles[i]->getChargeRatio()
        << std::scientific   << std::setw(16) << particles[i]->getNormalizedVel() << std::endl;
    }
    std::cout << std::endl;
}

void getMaxwellian(particle **particles, double temperature){
    //temperature [eV]
    double sigma = sqrt(__ELEMENT_CHARGE__*temperature/__ELECTRON_MASS__);
    const gsl_rng_type *T;
    gsl_rng *r;
    gsl_rng_env_setup();

    T = gsl_rng_default;
    r = gsl_rng_alloc(T);

    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++){
        particles[i]->setNormalizedVel(gsl_ran_gaussian(r, sigma));
    }
    gsl_rng_free(r);
}

void getInitialParticles(particle **particles, grid &grid){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++){
        // double x0 = domainSize/4.0;
        // particles[i] = new particle(__ELECTRON_MASS__, -__ELEMENT_CHARGE__, x0, 0.0, 0.0, __DEFAULT_WEIGHT__);


        /*
         * get evenly distributed particle positions
         */ 
        // double x0 = domainSize/(__NUMBER_OF_PARTICLES__+1)*(i+1);
        // particles[i] = new particle(__ELECTRON_MASS__, -__ELEMENT_CHARGE__, x0, 0.0, 0.0, __DEFAULT_WEIGHT__);

        /*
         * get evenly distributed particle positions with a perturbation
         */ 
        double x0;
        x0 = grid.getDomainSize()/(__NUMBER_OF_PARTICLES__)*(i+0.5);
        x0 = grid.getDomainSize()*(i+0.5)/__NUMBER_OF_PARTICLES__;
        x0 += 0.001*cos(2*M_PI*x0/grid.getDomainSize());
        particles[i] = new particle(1.0, -1.0, x0, 0.0, 0.0, __DEFAULT_WEIGHT__);

        /*
         * get initial particle positions for the 'two particle test'
         */
        // double x0;
        // if(i%2) {
        //     x0 = grid.getDomainSize()*0.5;
        //     particles[i] = new particle(1.0, -1.0, x0, 0.0, 0.0, __DEFAULT_WEIGHT__);
        // }
        // else {
        //     x0 = grid.getDomainSize()/4.0;
        //     particles[i] = new particle(1.0, -1.0, x0, 0.0, 0.0, __DEFAULT_WEIGHT__);
        // }

        // double x0;
        // x0 = grid.getDomainSize()*(i+1)/4.0;
        // particles[i] = new particle(1.0, -1.0, x0, 0.0, 0.0, __DEFAULT_WEIGHT__);
    }
}

void updateParticlesIndex(particle **particles, grid &grid){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++){
        int index = static_cast <int> (floor(particles[i]->getNormalizedPos()/__DELTA_X__));
        if(particles[i]->getNormalizedPos() == grid.getNormalizedPos(index)){
            particles[i]->setIndex(index);
            particles[i]->setWeightLeft(1.0);
            particles[i]->setWeightRight(0.0);
        }
        else if(particles[i]->getNormalizedPos() == grid.getNormalizedPos(index+1)){
            particles[i]->setIndex(index);
            particles[i]->setWeightLeft(0.0);
            particles[i]->setWeightRight(1.0);
        }
        else{
            double weight = (particles[i]->getNormalizedPos() - grid.getNormalizedPos(index)) / __DELTA_X__;
            particles[i]->setIndex(index);
            particles[i]->setWeightLeft(1.0 - weight);
            particles[i]->setWeightRight(weight);
        }
    }
}

void updateParticlesPeriodic(particle **particles, grid &grid){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++){
        particles[i]->checkBoundary(grid.getUpperLimit(), grid.getLowerLimit());
    }
}

void updateParticlesAcceleration(particle **particles, grid &grid){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++){
        double normalizedEfield = 0;
        double acceleration;
        int index = particles[i]->getIndex();
        normalizedEfield += particles[i]->getWeightLeft() *grid.getNormalizedEfield(index);
        normalizedEfield += particles[i]->getWeightRight()*grid.getNormalizedEfield(index+1);
        acceleration = normalizedEfield*particles[i]->getChargeRatio()/particles[i]->getMassRatio();
        particles[i]->setNormalizedAccel(acceleration);
    }
}

void updateParticlesVelocity(particle **particles, double timeDifference){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++) particles[i]->doAccel(timeDifference);
}

void updateParticlesPosition(particle **particles, double timeDifference){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++) particles[i]->doMove(timeDifference);
}

