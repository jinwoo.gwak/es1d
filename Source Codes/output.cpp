#include "define.h"
#include "output.h"

output::output()
    : fileId(H5Fcreate(HDF5_FILE_NAME, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT))
    , ptlGroup(H5Gcreate2(fileId, "/particle data", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT))
    , gridGroup(H5Gcreate2(fileId, "/grid data", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)){
    _time_index = 0;
    dataSpace0D = H5Screate(H5S_SCALAR);
    dataSetDim1D[0]    = __NUMBER_OF_STEPS__;
    dataSpace1D_time   = H5Screate_simple(1, dataSetDim1D, NULL);
    dataSetDim1D[0]    = __NUMBER_OF_NODES__;
    dataSpace1D_node   = H5Screate_simple(1, dataSetDim1D, NULL);
    dataSetDim2D[0]    = __NUMBER_OF_STEPS__;
    dataSetDim2D[1]    = __NUMBER_OF_PARTICLES__;
    dataSpace2D_ptl    = H5Screate_simple(2, dataSetDim2D, NULL);
    dataSetDim2D[0]    = __NUMBER_OF_STEPS__;
    dataSetDim2D[1]    = __NUMBER_OF_NODES__;
    dataSpace2D_node   = H5Screate_simple(2, dataSetDim2D, NULL);
    dataSetDim2D[0]    = 1;
    dataSetDim2D[1]    = __NUMBER_OF_PARTICLES__;
    memorySpace2D_ptl  = H5Screate_simple(2, dataSetDim2D, NULL);
    dataSetDim2D[0]    = 1;
    dataSetDim2D[1]    = __NUMBER_OF_NODES__;
    memorySpace2D_node = H5Screate_simple(2, dataSetDim2D, NULL);
    for(int i=0; i<__NUMBER_OF_STEPS__; i++) _val_time_domain[i] = i*__DELTA_T__;
    for(int i=0; i<__NUMBER_OF_NODES__; i++) _val_node_domain[i] = i*__DELTA_X__;
    attributeType = H5Tcopy(H5T_C_S1);
    H5Tset_size(attributeType, 50);
    H5Tset_strpad(attributeType, H5T_STR_NULLTERM);
    dataSet = H5Dcreate2(fileId, "TIME_DOMAIN", H5T_NATIVE_DOUBLE, dataSpace1D_time, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, _val_time_domain);
    H5Awrite(attribute, attributeType, "[Normalized] t = t*omega_p");

    dataSet = H5Dcreate2(fileId, "NODE_DOMAIN", H5T_NATIVE_DOUBLE, dataSpace1D_node, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, _val_node_domain);
    H5Awrite(attribute, attributeType, "[Normalized] x = x/lambda");

    _val_double = __DEFAULT_WEIGHT__;
    dataSet   = H5Dcreate2(fileId, "__DEFAULT_WEIGHT__", H5T_NATIVE_DOUBLE, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_double);
    H5Awrite(attribute, attributeType, "[# m^(-2)]");

    _val_double = __ELEMENT_CHARGE__;
    dataSet   = H5Dcreate2(fileId, "__ELEMENT_CHARGE__", H5T_NATIVE_DOUBLE, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_double);
    H5Awrite(attribute, attributeType, "[C]");

    _val_double = __ELECTRON_MASS__;
    dataSet   = H5Dcreate2(fileId, "__ELECTRON_MASS__", H5T_NATIVE_DOUBLE, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_double);
    H5Awrite(attribute, attributeType, "[kg]");

    _val_double = __EPSILON_ZERO__;
    dataSet   = H5Dcreate2(fileId, "__EPSILON_ZERO__", H5T_NATIVE_DOUBLE, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_double);
    H5Awrite(attribute, attributeType, "[N V^(-2))]");

    _val_double = __H2_MASS__;
    dataSet   = H5Dcreate2(fileId, "__H2_MASS__", H5T_NATIVE_DOUBLE, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_double);
    H5Awrite(attribute, attributeType, "[kg]");

    _val_int = __NUMBER_OF_PARTICLES__;
    dataSet   = H5Dcreate2(fileId, "__NUMBER_OF_PARTICLES__", H5T_NATIVE_INT, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_int);

    _val_int = __NUMBER_OF_STEPS__;
    dataSet = H5Dcreate2(fileId, "__NUMBER_OF_STEPS__", H5T_NATIVE_INT, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_int);

    _val_int = __NUMBER_OF_GRIDS__;
    dataSet = H5Dcreate2(fileId, "__NUMBER_OF_GRIDS__", H5T_NATIVE_INT, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_int);

    _val_int = __NUMBER_OF_NODES__;
    dataSet = H5Dcreate2(fileId, "__NUMBER_OF_NODES__", H5T_NATIVE_INT, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_int);

    _val_double = __DELTA_X__;
    dataSet = H5Dcreate2(fileId, "__DELTA_X__", H5T_NATIVE_DOUBLE, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_double);
    H5Awrite(attribute, attributeType, "[Normalized] dx = dx/lambda");

    _val_double = __DELTA_T__;
    dataSet = H5Dcreate2(fileId, "__DELTA_T__", H5T_NATIVE_DOUBLE, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_double);
    H5Awrite(attribute, attributeType, "[Normalized] dt = dt*omega_p");

    _val_double = sqrt(pow(__ELEMENT_CHARGE__,2)*__DEFAULT_WEIGHT__*__NUMBER_OF_PARTICLES__/__NUMBER_OF_GRIDS__/__DELTA_X__/__ELECTRON_MASS__/__EPSILON_ZERO__);
    dataSet = H5Dcreate2(fileId, "plasma frequency", H5T_NATIVE_DOUBLE, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &_val_double);
    attribute = H5Acreate2(dataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute, attributeType, "[sec^(-1)]");

    ptlPosDataSet      = H5Dcreate2(ptlGroup,  "normalized particle position",               H5T_NATIVE_DOUBLE, dataSpace2D_ptl,  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    ptlVelDataSet      = H5Dcreate2(ptlGroup,  "normalized particle velocity",               H5T_NATIVE_DOUBLE, dataSpace2D_ptl,  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    nodeEnergyDataSet  = H5Dcreate2(gridGroup, "normalized node electrostatic field energy", H5T_NATIVE_DOUBLE, dataSpace2D_node, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    nodeEfieldDataSet  = H5Dcreate2(gridGroup, "normalized node electric field",             H5T_NATIVE_DOUBLE, dataSpace2D_node, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    nodeRhoDataSet     = H5Dcreate2(gridGroup, "normalized node charge density",             H5T_NATIVE_DOUBLE, dataSpace2D_node, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    nodePhiDataSet     = H5Dcreate2(gridGroup, "normalized node potential",                  H5T_NATIVE_DOUBLE, dataSpace2D_node, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    
    attribute = H5Acreate2(ptlPosDataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute, attributeType, "[Normalized] x = x/lambda");
    
    attribute = H5Acreate2(ptlVelDataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute, attributeType, "[Normalized] v = v/lambda/omega_p");

    attribute = H5Acreate2(nodeEnergyDataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute, attributeType, "[Normalized] ESE = ESE/n_0/lambda^3/omega_p^2/m_e/dy/dz");

    attribute = H5Acreate2(nodeEfieldDataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute, attributeType, "[Normalized] E = E*|e|/lambda/omega_p^2/m_e");
    
    attribute = H5Acreate2(nodeRhoDataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute, attributeType, "[Normalized] Rho = Rho/|e|/n_0");
    
    attribute = H5Acreate2(nodePhiDataSet, "Unit", attributeType, dataSpace0D, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute, attributeType, "[Normalized] Phi = Phi*|e|/lambda^2/omega_p^2/m_e");
}

void output::updateTimeIndex(){
    _time_index++;
}

void output::writePtlPosition(particle **particles){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++) _val_ptl_domain[i] = particles[i]->getNormalizedPos();
    offset[0] = _time_index;
    offset[1] = 0;
    count[0] = 1;
    count[1] = __NUMBER_OF_PARTICLES__;
    stride[0] = 1;
    stride[1] = 1;
    block[0] = 1;
    block[1] = 1;
    H5Sselect_hyperslab(dataSpace2D_ptl, H5S_SELECT_SET, offset, stride, count, block);
    H5Dwrite(ptlPosDataSet, H5T_NATIVE_DOUBLE, memorySpace2D_ptl, dataSpace2D_ptl, H5P_DEFAULT, _val_ptl_domain);
}

void output::writePtlVelocity(particle **particles){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++) _val_ptl_domain[i] = particles[i]->getNormalizedVel();
    offset[0] = _time_index;
    offset[1] = 0;
    count[0] = 1;
    count[1] = __NUMBER_OF_PARTICLES__;
    stride[0] = 1;
    stride[1] = 1;
    block[0] = 1;
    block[1] = 1;
    H5Sselect_hyperslab(dataSpace2D_ptl, H5S_SELECT_SET, offset, stride, count, block);
    H5Dwrite(ptlVelDataSet, H5T_NATIVE_DOUBLE, memorySpace2D_ptl, dataSpace2D_ptl, H5P_DEFAULT, _val_ptl_domain);
}

void output::writeNodeEnergy(grid &grid){
    for(int i=0; i<__NUMBER_OF_NODES__; i++){
        _val_node_domain[i] = 0.5*__DELTA_X__*grid.getNormalizedPhi(i)*grid.getNormalizedRho(i);
    }
    offset[0] = _time_index;
    offset[1] = 0;
    count[0] = 1;
    count[1] = __NUMBER_OF_NODES__;
    stride[0] = 1;
    stride[1] = 1;
    block[0] = 1;
    block[1] = 1;
    H5Sselect_hyperslab(dataSpace2D_node, H5S_SELECT_SET, offset, stride, count, block);
    H5Dwrite(nodeEnergyDataSet, H5T_NATIVE_DOUBLE, memorySpace2D_node, dataSpace2D_node, H5P_DEFAULT, _val_node_domain);
}

void output::writeNodeEfield(grid &grid){
    for(int i=0; i<__NUMBER_OF_NODES__; i++) _val_node_domain[i] = grid.getNormalizedEfield(i);
    offset[0] = _time_index;
    offset[1] = 0;
    count[0] = 1;
    count[1] = __NUMBER_OF_NODES__;
    stride[0] = 1;
    stride[1] = 1;
    block[0] = 1;
    block[1] = 1;
    H5Sselect_hyperslab(dataSpace2D_node, H5S_SELECT_SET, offset, stride, count, block);
    H5Dwrite(nodeEfieldDataSet, H5T_NATIVE_DOUBLE, memorySpace2D_node, dataSpace2D_node, H5P_DEFAULT, _val_node_domain);
}

void output::writeNodeRho(grid &grid){
    for(int i=0; i<__NUMBER_OF_NODES__; i++) _val_node_domain[i] = grid.getNormalizedRho(i);
    offset[0] = _time_index;
    offset[1] = 0;
    count[0] = 1;
    count[1] = __NUMBER_OF_NODES__;
    stride[0] = 1;
    stride[1] = 1;
    block[0] = 1;
    block[1] = 1;
    H5Sselect_hyperslab(dataSpace2D_node, H5S_SELECT_SET, offset, stride, count, block);
    H5Dwrite(nodeRhoDataSet, H5T_NATIVE_DOUBLE, memorySpace2D_node, dataSpace2D_node, H5P_DEFAULT, _val_node_domain);
}

void output::writeNodePhi(grid &grid){
    for(int i=0; i<__NUMBER_OF_NODES__; i++) _val_node_domain[i] = grid.getNormalizedPhi(i);
    offset[0] = _time_index;
    offset[1] = 0;
    count[0] = 1;
    count[1] = __NUMBER_OF_NODES__;
    stride[0] = 1;
    stride[1] = 1;
    block[0] = 1;
    block[1] = 1;
    H5Sselect_hyperslab(dataSpace2D_node, H5S_SELECT_SET, offset, stride, count, block);
    H5Dwrite(nodePhiDataSet, H5T_NATIVE_DOUBLE, memorySpace2D_node, dataSpace2D_node, H5P_DEFAULT, _val_node_domain);
}

output::~output(){
    std::cout << "Called : hdf5 Destructor" << std::endl; 
    // close the attribute
    H5Aclose(attribute);

    // close the memory space
    H5Sclose(memorySpace2D_node);
    H5Sclose(memorySpace2D_ptl);

    // close the dataSpaces
    H5Sclose(dataSpace0D);
    H5Sclose(dataSpace1D_time);
    H5Sclose(dataSpace1D_node);
    H5Sclose(dataSpace2D_ptl);
    H5Sclose(dataSpace2D_node);
    

    // close the data-set
    H5Dclose(dataSet);
    H5Dclose(ptlPosDataSet);
    H5Dclose(ptlVelDataSet);
    H5Dclose(nodeEnergyDataSet);
    H5Dclose(nodeEfieldDataSet);
    H5Dclose(nodeRhoDataSet);
    H5Dclose(nodePhiDataSet);

    // close the dataTypes
    H5Tclose(attributeType);

    // close the groups
    H5Gclose(ptlGroup);
    H5Gclose(gridGroup);

    // terminate access to the file
    H5Fclose(fileId);
}