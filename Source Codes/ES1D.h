#include "output.h"

void showParticlesPosition(particle **particles);
void showParticlesVelocity(particle **particles);
void getMaxwellian(particle **particles, double temperature);
void getInitialParticles(particle **particles, grid &grid);
void updateParticlesIndex(particle **particles, grid &grid);
void updateParticlesPeriodic(particle **particles, grid &grid);
void updateParticlesAcceleration(particle **particles, grid &grid);
void updateParticlesVelocity(particle **particles, double timeDifference);
void updateParticlesPosition(particle **particles, double timeDifference);
