#include "define.h"
#include "ES1D.h"

int main() {
    auto start = std::chrono::system_clock::now();
    std::time_t start_time = std::chrono::system_clock::to_time_t(start);
    std::cout << std::ctime(&start_time) << std::endl;

    // create a output object
    output hdf5Output = output();

    // declare grid object
    grid grid(__NUMBER_OF_NODES__, __DELTA_X__);
    
    // declare array of pointers to particle object. and initialize it using nullptr
    particle *particles[__NUMBER_OF_PARTICLES__];
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++){ particles[i] = nullptr; }

    // initialize particles
    getInitialParticles(particles, grid);
    updateParticlesPeriodic(particles, grid);
    updateParticlesIndex(particles, grid);
    grid.addNormalizedRho(particles);
    grid.addPeriodicRho();
    grid.addBackgroundRho(1.0);
    grid.solvePoisson();
    hdf5Output.writePtlPosition(particles);
    hdf5Output.writePtlVelocity(particles);
    hdf5Output.writeNodeEfield(grid);
    hdf5Output.writeNodeEnergy(grid);
    hdf5Output.writeNodePhi(grid);
    hdf5Output.writeNodeRho(grid);
    hdf5Output.updateTimeIndex();
    grid.clearNormalizedRho();


    // using euler's method, advance the velocity by half step ( i.e. dt/2 )
    updateParticlesAcceleration(particles, grid);
    updateParticlesVelocity(particles, __DELTA_T__/2.0);
    updateParticlesPosition(particles, __DELTA_T__);
    updateParticlesPeriodic(particles, grid);
    updateParticlesIndex(particles, grid);
    grid.addNormalizedRho(particles);
    grid.addPeriodicRho();
    grid.addBackgroundRho(1.0);
    grid.solvePoisson();
    hdf5Output.writePtlPosition(particles);
    hdf5Output.writePtlVelocity(particles);
    hdf5Output.writeNodeEfield(grid);
    hdf5Output.writeNodeEnergy(grid);
    hdf5Output.writeNodePhi(grid);
    hdf5Output.writeNodeRho(grid);
    hdf5Output.updateTimeIndex();
    grid.clearNormalizedRho();
    
    
    // main loop
    for(int i=2; i<__NUMBER_OF_STEPS__; i++){
        updateParticlesAcceleration(particles, grid);
        updateParticlesVelocity(particles, __DELTA_T__);
        updateParticlesPosition(particles, __DELTA_T__);
        updateParticlesPeriodic(particles, grid);
        updateParticlesIndex(particles, grid);        
        grid.addNormalizedRho(particles);
        grid.addPeriodicRho();
        grid.addBackgroundRho(1.0);
        grid.solvePoisson();
        hdf5Output.writePtlPosition(particles);
        hdf5Output.writePtlVelocity(particles);
        hdf5Output.writeNodeEfield(grid);
        hdf5Output.writeNodeEnergy(grid);
        hdf5Output.writeNodePhi(grid);
        hdf5Output.writeNodeRho(grid);
        hdf5Output.updateTimeIndex();
        grid.clearNormalizedRho();
    }

    // delete the dynamically allocated particle objects
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++){
        delete particles[i];
    }

    /*
    int numPtl2 = 1E+4;
    double temperature = 5; //5[eV]
    double *ptlVelocity = new double [numPtl2];
    getMaxwell(numPtl2, temperature, ptlVelocity);
    printVelocityDistribution(numPtl2, ptlVelocity);
    */

    return 0;
}