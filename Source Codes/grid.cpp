#include "define.h"
#include "grid.h"

grid::grid() 
    : _numNode(2) 
    , _numGrid(1)
    , _deltaX(1.0)
    , _domainSize(1.0)
    , _lowerLimit(0.0)
    , _upperLimit(1.0) {
    _normalizedPos    = new double[2]();
    _normalizedRho    = new double[2]();
    _normalizedPhi    = new double[2]();
    _normalizedEfield = new double[2]();
    _K = new double[1]();
    _fftInput  = (fftw_complex *)fftw_malloc(_numGrid*sizeof(fftw_complex));
    _fftOutput = (fftw_complex *)fftw_malloc(_numGrid*sizeof(fftw_complex)); 
    _normalizedPos[0] = 0.0;
    _normalizedPos[1] = 1.0;
    _K[0] = 0.0;
} 

grid::grid(const int numNode, const double deltaX) 
    : _numNode(numNode)
    , _numGrid(numNode-1)
    , _deltaX(deltaX)
    , _domainSize(_deltaX*_numGrid)
    , _lowerLimit(0.0)
    , _upperLimit(_deltaX*_numGrid) {    
    _normalizedPos    = new double[_numNode]();
    _normalizedRho    = new double[_numNode]();
    _normalizedPhi    = new double[_numNode]();
    _normalizedEfield = new double[_numNode]();
    _K = new double[_numGrid]();
    _fftInput  = (fftw_complex *)fftw_malloc(_numGrid*sizeof(fftw_complex));
    _fftOutput = (fftw_complex *)fftw_malloc(_numGrid*sizeof(fftw_complex)); 
    for(int i=0; i<_numNode; i++){ 
        _normalizedPos[i] = i*_deltaX; 
    }
    for(int i=0; i<_numGrid; i++){
        double _k = 2*M_PI*i/_domainSize;
        _K[i] = _k*sin(_k*deltaX/2.0)/(_k*deltaX/2.0);
    }
}

grid::grid(const int numNode, const double deltaX, const double X0) 
    : _numNode(numNode)
    , _numGrid(numNode-1)
    , _deltaX(deltaX)
    , _domainSize(_deltaX*_numGrid)
    , _lowerLimit(X0)
    , _upperLimit(X0+_deltaX*_numGrid) {    
    _normalizedPos    = new double[_numNode]();
    _normalizedRho    = new double[_numNode]();
    _normalizedPhi    = new double[_numNode]();
    _normalizedEfield = new double[_numNode]();
    _K = new double[_numGrid]();
    _fftInput  = (fftw_complex *)fftw_malloc(_numGrid*sizeof(fftw_complex));
    _fftOutput = (fftw_complex *)fftw_malloc(_numGrid*sizeof(fftw_complex)); 
    for(int i=0; i<numNode; i++){ 
        _normalizedPos[i] = i*_deltaX + X0; 
    }
    for(int i=0; i<_numGrid; i++){
        double _k = 2*M_PI*i/_domainSize;
        _K[i] = _k*sin(_k*deltaX/2.0)/(_k*deltaX/2.0);
    }
}

int grid::getNumNode() const { 
    return _numNode; 
}

int grid::getNumGrid() const { 
    return _numGrid; 
}

double grid::getDeltaX() const { 
    return _deltaX; 
}

double grid::getDomainSize() const {
    return _domainSize;
}

double grid::getLowerLimit() const {
    return _lowerLimit;
}

double grid::getUpperLimit() const {
    return _upperLimit;
}

double grid::getNormalizedPos(const int index) const { 
    return _normalizedPos[index]; 
}

double grid::getNormalizedRho(const int index) const { 
    return _normalizedRho[index]; 
}

double grid::getNormalizedPhi(const int index) const { 
    return _normalizedPhi[index]; 
}

double grid::getNormalizedEfield(const int index) const { 
    return _normalizedEfield[index]; 
}

void grid::showNormalizedRho() const {
    std::cout << "\n";
    std::cout << std::right << std::scientific;
    std::cout << "---show Charge Density---" << std::endl;
    for(int i=0; i<_numNode; i++){
        std::cout << std::setw(18) << _normalizedRho[i] << std::endl;
    }
    std::cout << std::endl;
}

void grid::showNormalizedEfield() const {
    std::cout << "\n";
    std::cout << std::right << std::scientific;
    std::cout << "---show Electric Field---" << std::endl;
    for(int i=0; i<_numNode; i++){
        std::cout << std::setw(18) << _normalizedEfield[i] << std::endl;
    }
    std::cout << std::endl;
}

void grid::showNormalizedPhi() const {
    std::cout << "\n";
    std::cout << std::right << std::scientific;
    std::cout << "---show ES Potential---" << std::endl;
    for(int i=0; i<_numNode; i++){
        std::cout << std::setw(18) << _normalizedPhi[i] << std::endl;
    }
    std::cout << std::endl;
}

void grid::addNormalizedRho(const int index, const double chargeDensity){
    _normalizedRho[index] += chargeDensity;
}

void grid::addNormalizedRho(particle** particles){
    for(int i=0; i<__NUMBER_OF_PARTICLES__; i++){
        int index = particles[i]->getIndex();
        // n   = W0/dX              :W0[#/m^2], dX[m]
        // n_0 = N*W0/(Ng*dX)       :N[#], Ng[#]
        // (q/|e|)*(n/n_0) = charge_ratio * (Ng / N)
        double charge_ratio  = particles[i]->getChargeRatio();
        double normalizedRho = charge_ratio * __NUMBER_OF_GRIDS__ / __NUMBER_OF_PARTICLES__;
        addNormalizedRho(index,   normalizedRho*particles[i]->getWeightLeft());
        addNormalizedRho(index+1, normalizedRho*particles[i]->getWeightRight());   
    }
}

void grid::addPeriodicRho(){
    addNormalizedRho(0,            getNormalizedRho(getNumGrid()));
    addNormalizedRho(getNumGrid(), getNormalizedRho(0) - getNormalizedRho(getNumGrid()));
}

void grid::addBackgroundRho(const double backGroundRho){
    for(int i=0; i<getNumNode(); i++) addNormalizedRho(i, backGroundRho);
}

void grid::clearNormalizedRho() { 
    for(int i=0; i<_numNode; i++) _normalizedRho[i] = 0; 
} 

void grid::clearNormalizedPhi() { 
    for(int i=0; i<_numNode; i++) _normalizedPhi[i] = 0; 
} 

void grid::clearNormalizedEfield() { 
    for(int i=0; i<_numNode; i++) _normalizedEfield[i] = 0; 
} 

void grid::solvePoisson() {
    // check the periodicity of _normalizedRho
    if(_normalizedRho[0] != _normalizedRho[_numGrid]) {
        std::cout << "Warning : FFT : periodicity of '_normalizedRho' is broken " << std::endl;
    }

    // create the 'fft_plan' variable
    _fftPlan = fftw_plan_dft_1d(_numGrid, _fftInput, _fftOutput, FFTW_FORWARD, FFTW_ESTIMATE);

    // initialize fft_input, note, the last element of _normalizedRho is omitted, _normalizedRho[_numGrid],
    // since it is redundant to the first element, _normalizedRho[0]
    for(int i=0; i<_numGrid; i++) {
        _fftInput[i][0] = _normalizedRho[i]; // real part
        _fftInput[i][1] = 0;                  // imaginary part
    }

    // do Fast Fourier Transform (i.e. FORWARD FFT)
    fftw_execute(_fftPlan);

    // check the charge neutrality of the system
    // double _super_particle_balance = abs(_fftOutput[0][0])/__ELEMENT_CHARGE__*__DELTA_X__/__DEFAULT_WEIGHT__;
    double _super_particle_balance = abs(_fftOutput[0][0]);
    if(_super_particle_balance > __FLT_EPSILON__){
        std::cout << std::scientific << std::right;
        std::cout << "Warning : FFT : charge neutrality is broken " << std::endl;
        std::cout << "_SUP. PTL BALANCE(%): " << std::setw(15) << _super_particle_balance << std::endl;
    }

    // re-initialize 'fft_plan' variables
    _fftPlan = fftw_plan_dft_1d(_numGrid, _fftInput, _fftOutput, FFTW_BACKWARD, FFTW_ESTIMATE);

    // do the algebraic operation to compute the electrostatic potential in frequency domain
    for(int i=1; i<_numGrid; i++) {
        _fftInput[i][0] = _fftOutput[i][0] / pow(_K[i], 2); // real part
        _fftInput[i][1] = _fftOutput[i][1] / pow(_K[i], 2); // imaginary part
    }
    
    // the amplitude of zero-th mode is zero. (i.e. charge neutrality)
    // this vaule should be zero to prevent zero-division issue.
    _fftInput[0][0] = 0;
    _fftInput[0][1] = 0;
   
    // do Inverse Fast Fourier Transform (i.e. BACKWARD FFT)
    fftw_execute(_fftPlan);

    for(int i=0; i<_numGrid; i++) {
        _normalizedPhi[i] = _fftOutput[i][0]/_numGrid;
        if(abs(_fftOutput[i][1])/abs(_fftOutput[i][0]) > 1E-6) { 
            std::cout << std::defaultfloat << std::endl;
            std::cout << "Warning : FFT : Too big imaginary part" << std::endl;
            std::cout << "|Im[phi_k(" << std::right << std::setw(3) << i << ")]|/|Re[phi_k(" << std::right << std::setw(3) << i << ")]|:  " 
            << std::scientific << std::right << std::setw(15) << abs(_fftOutput[i][1])/abs(_fftOutput[i][0]) << std::endl;
        }
    }

    // compute the electric field at the boundary nodes.
    _normalizedEfield[0]          = (_normalizedPhi[_numGrid-1] - _normalizedPhi[1])/(2.0*_deltaX);
    _normalizedEfield[_numGrid-1] = (_normalizedPhi[_numGrid-2] - _normalizedPhi[0])/(2.0*_deltaX);

    // periodicity of the domain gives following condition
    // remember, the last element, my_grid_variable[_numGrid] is always redundant to the first element, my_grid_variable[0].
    _normalizedEfield[_numGrid]   = _normalizedEfield[0];
    _normalizedPhi[_numGrid]      = _normalizedPhi[0];

    for(int i=1; i<_numGrid-1; i++) {
        _normalizedEfield[i] = (_normalizedPhi[i-1] - _normalizedPhi[i+1])/(2.0*_deltaX);
    }

    for(int i=0; i<_numGrid; i++){
        _fftInput[i][0] = 0.0;
        _fftInput[i][1] = 0.0;
        _fftOutput[i][0] = 0.0;
        _fftOutput[i][1] = 0.0;
    }
}

grid::~grid(){
    std::cout << "Called : grid Desturctor" << std::endl;

    // free the dynamically allocated memory
    delete[] _normalizedPos;
    delete[] _normalizedRho;          
    delete[] _normalizedEfield;
    delete[] _K;

    // free the FFT variables
    fftw_destroy_plan(_fftPlan);
    fftw_free(_fftInput);
    fftw_free(_fftOutput);
}