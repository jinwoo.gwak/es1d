#include <iostream>
#include <iomanip>
#include <chrono>
#define _USE_MATH_DEFINES
#include <cmath>
#include <fstream>
#include <fftw3.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <hdf5.h>

#define GSL_RNG_SEED   0

#define __NUMBER_OF_PARTICLES__ 128
#define __NUMBER_OF_NODES__     33
#define __NUMBER_OF_GRIDS__     32
#define __NUMBER_OF_STEPS__     150
#define __DELTA_X__             (2*M_PI/32.0)    // 12.2718463030851e-003
#define __DELTA_T__             0.2

#define __DEFAULT_WEIGHT__      1.0                   // [#/m^2]
#define __ELEMENT_CHARGE__      1.602176634E-19       // 1.0
#define __ELECTRON_MASS__       9.1093837015E-31      // 1.0
#define __EPSILON_ZERO__        8.8541878128E-12      // 1.0
#define __H2_MASS__             3.3452E-27

#define HDF5_FILE_NAME         "../OUTPUT/OUTPUT.h5"
