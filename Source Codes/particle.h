class particle{
private:
    /**
     * @brief Super partcle class
     * 
     * _totalNumPtl      count the total number of particle objects.
     *                   this variable is shared by the all particle objects.
     *  
     * _massRatio        m/m_e, ratio of mass of the particle to the mass of electron, 
     *                   ( e.g. for a proton, _massRatio = 1836 )
     *  
     * _chargeRatio      q/|e|, ratio of charge of the particle to the elementary charge,
     *                   ( e.g. for a proton and a electron, +1.0 and -1.0 respectively )
     *  
     * _normalizedPos    particle position normalized by the debye length 
     *                   ( i.e. _normalizedPos = x/lambda )
     *  
     * _normalizedVel    particle veloicty normalized by the debye length and plasma frequency
     *                   ( i.e. _normalizedVel = v/lambda/omega_p)
     *  
     * _normalizedPreVel normalized velocity of the particle object in the previous time step
     *  
     * _normalizedAccel  particle acceleration normalized by the debye length and plasma frequency
     *                   ( i.e. _normalizedAccel = a/lambda/omega_p^2 )
     *  
     * _index            index for the nearest node on left side of the particle 
     *                   ( i.e. X[_index] ≤ _normalizedPos < X[_index+1])
     *  
     * _weightLeft       weight for the nearest left node 
     *  
     * _weightRight      weight for the nearest right node. It is one's complementary of '_weightLeft'
     *                   ( i.e. _weightRight = 1.0 - _weightLeft )
     *  
     * _weightPtl        weight of the particle object
     *                   It plays same role with the N_s( Capital 'N' with subscript 's' ) 
     *                   in eq. 2-32 from R. W. Hockney, 1988, 31p. 
     */
    static int _totalNumPtl; 
    const double _massRatio;
    const double _chargeRatio;
    double _normalizedPos;
    double _normalizedVel;
    double _normalizedPreVel;
    double _normalizedAccel;
    double _weightLeft;
    double _weightRight;
    double _weightPtl;
    int _index;   
public:
    /**
     * @brief Construct a new particle object, default constructor
     * 
     */
    particle();                                
    
    /**
     * @brief Constructor a new particle object
     *  
     * @param massRatio       m/m_e
     * @param chargeRatio     q/|e|
     */
    particle(const double massRatio, const double chargeRatio);

    /**
     * @brief Construct a new particle object
     * 
     * @param massRatio       m/m_e
     * @param chargeRatio     q/|e|
     * @param normalizedPos   initial normalized position
     * @param normalizedVel   initial normalized velocity
     * @param normalizedAccel initial normalized acceleration
     * @param weightPtl       particle weight
     */
    particle(const double massRatio, const double chargeRatio, const double normalizedPos, const double normalizedVel, const double normalizedAccel, const double weightPtl);

    /**
     * @brief Copy constructor
     * 
     * @param ptl             particle objct which will be copied
     */
    particle(const particle& ptl);

    /**
     * @brief Get the total number of the existing particle objects
     * 
     * @return number of particles in the simulation
     */
    static int getTotalNumPtl();

    /**
     * @brief Get the mass ratio( i.e. m/m_e )
     * 
     * @return mass ratio of the particle
     */
    double getMassRatio() const;

    /**
     * @brief Get the charge ratio( i.e. q/|e| )
     * 
     * @return charge ratio of the particle 
     */
    double getChargeRatio() const;

    /**
     * @brief Get the normalized position
     * 
     * @return normalized position of the particle 
     */
    double getNormalizedPos() const;
    
    /**
     * @brief Get the normalized velocity
     * 
     * @return normalized velocity of the particle
     */
    double getNormalizedVel() const;

    /**
     * @brief Get the normalized velocity in the previous time step
     * 
     * @return normalized velocity of the particle object in the previous time step
     */
    double getNormalizedPreVel() const;

    /**
     * @brief Get the normalized acceleration
     * 
     * @return normalized acceleration of the particle
     */
    double getNormalizedAccel() const;

    
    /**
     * @brief Get the grid index of the nearest node on left side 
     * 
     * @return index of the nearest node on left side
     */
    int getIndex() const;

    /**
     * @brief Get the weight for the left node
     * 
     * @return weight for the left nearest node
     */
    double getWeightLeft() const;

    /**
     * @brief Get the weight for the right node
     * 
     * @return weight for the right nearest node
     */
    double getWeightRight() const;

    /**
     * @brief Get the particle weight. [#/m^2]
     * 
     * @return particle weight
     */
    double getWeightPtl() const;

    /**
     * @brief Set the index 
     * 
     * @param index 
     */
    void setIndex(const int index);

    /**
     * @brief Set the weight for the left-nearest node
     * 
     * @param weight 
     */
    void setWeightLeft(const double weight);

    /**
     * @brief Set the weight for the right-nearest node
     * 
     * @param weight 
     */
    void setWeightRight(const double weight);

    /**
     * @brief Set the normalized position of the particle object
     * 
     * Normalization 
     * x = x/lambda
     * 
     * @param normalizedPos   particle position in normalized unit
     */
    void setNormalizedPos(const double normalizedPos);

    /**
     * @brief Set the normalized velocity of the particle object
     * 
     * Normalization
     * v = v/lambda/omega_p
     * 
     * @param normalizedVel   particlce velocity in normalized unit
     */
    void setNormalizedVel(const double normalizedVel);

    /**
     * @brief Update(or Set) the normalized particle velocity in the previous time step
     * 
     * @param normalizedPreVel particle velocity in normalized unit
     */
    void setNormalizedPreVel(const double normalizedPreVel);

    /**
     * @brief Set the normalized acceleration of the particle 
     * 
     * Normalization
     * a = (|e|E/m_e/lambda/omega_p^2) * (q/|e|) / (m/m_e)
     * 
     * @param normalizedAccel particle acceleration in normalized unit
     */
    void setNormalizedAccel(const double normalizedAccel);

    /**
     * @brief advance the normalized velocity by the given time difference
     * 
     * the amount of advance is determined by '_normalizedAccel' variable,
     * via 'setNormalizedAccel' function
     * 
     * @param deltaTime       the amount of the time step 
     */
    void doAccel(const double deltaTime);

    /**
     * @brief advance the normalized position by the given time difference
     * 
     * the amount of advance is determined by '_normalizedVel' variable,
     * via 'doAccel' function 
     * 
     * @param deltaTime 
     */
    void doMove(const double deltaTime);

    /**
     * @brief check and force the particle to be in the simulation domain
     * 
     * @param upperBoundary upper boundary of the simulation domain in normalized unit
     * @param lowerBoundary lower boundary of the simulation domain in normalized unit
     */
    void checkBoundary(const double upperBoundary, const double lowerBoundary);

    /**
     * @brief Destroy the particle object
     * 
     * decreate the '_totalNumPtl' static variable by 1.
     * 
     */
    ~particle();
};