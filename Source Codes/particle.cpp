#include "define.h"
#include "particle.h"

int particle::_totalNumPtl = 0;

particle::particle()
    : _massRatio(1.0)
    , _chargeRatio(-1.0)
    , _normalizedPos(0.0)
    , _normalizedVel(0.0)
    , _normalizedPreVel(0.0)
    , _normalizedAccel(0.0)
    , _index(0)
    , _weightLeft(1.0)
    , _weightRight(0.0) 
    , _weightPtl(__DEFAULT_WEIGHT__){ 
    _totalNumPtl++;
}

particle::particle(const double massRatio, const double chargeRatio)
    : _massRatio(massRatio)
    , _chargeRatio(chargeRatio)
    , _normalizedPos(0.0)
    , _normalizedVel(0.0)
    , _normalizedPreVel(0.0)
    , _normalizedAccel(0.0)
    , _index(0)
    , _weightLeft(1.0)
    , _weightRight(0.0)
    , _weightPtl(__DEFAULT_WEIGHT__){ 
    _totalNumPtl++;
}

particle::particle(const double massRatio, const double chargeRatio, const double normalizedPos, const double normalizedVel, const double normalizedAccel, const double weightPtl)
    : _massRatio(massRatio)
    , _chargeRatio(chargeRatio)
    , _normalizedPos(normalizedPos)
    , _normalizedVel(normalizedVel)
    , _normalizedPreVel(normalizedVel)
    , _normalizedAccel(normalizedAccel)
    , _index(0)
    , _weightLeft(1.0)
    , _weightRight(0.0)
    , _weightPtl(weightPtl){ 
    _totalNumPtl++;
}

particle::particle(const particle& ptl)
    : _massRatio(ptl._massRatio)
    , _chargeRatio(ptl._chargeRatio)
    , _normalizedPos(ptl._normalizedPos)
    , _normalizedVel(ptl._normalizedVel)
    , _normalizedPreVel(ptl._normalizedPreVel)
    , _normalizedAccel(ptl._normalizedAccel)
    , _index(ptl._index)
    , _weightLeft(ptl._weightLeft)
    , _weightRight(ptl._weightRight)
    , _weightPtl(ptl._weightPtl){ 
    _totalNumPtl++;
}

int particle::getTotalNumPtl(){ 
    return _totalNumPtl; 
}

double particle::getMassRatio() const { 
    return _massRatio; 
}

double particle::getChargeRatio() const { 
    return _chargeRatio; 
}

double particle::getNormalizedPos() const { 
    return _normalizedPos; 
}

double particle::getNormalizedVel() const { 
    return _normalizedVel; 
}

double particle::getNormalizedPreVel() const {
    return _normalizedPreVel;
}

double particle::getNormalizedAccel() const { 
    return _normalizedAccel; 
}

int particle::getIndex() const { 
    return _index; 
}

double particle::getWeightLeft() const { 
    return _weightLeft; 
}

double particle::getWeightRight() const { 
    return _weightRight; 
}

double particle::getWeightPtl() const { 
    return _weightPtl; 
}

void particle::setIndex(const int index){ 
    _index = index; 
}

void particle::setWeightLeft(const double weight){ 
    _weightLeft = weight; 
}

void particle::setWeightRight(const double weight){ 
    _weightRight = weight; 
}

void particle::setNormalizedPos(const double normalizedPos){
    _normalizedPos = normalizedPos;
}

void particle::setNormalizedVel(const double normalizedVel){
    _normalizedVel = normalizedVel;
}

void particle::setNormalizedPreVel(const double normalizedPreVel){
    _normalizedPreVel = normalizedPreVel;
}

void particle::setNormalizedAccel(const double normalizedAccel){ 
    _normalizedAccel = normalizedAccel; 
}

void particle::doAccel(const double deltaTime){ 
    _normalizedPreVel = _normalizedVel;
    _normalizedVel += deltaTime*_normalizedAccel; 
}

void particle::doMove(const double deltaTime){ 
    _normalizedPos += deltaTime*_normalizedVel; 
}

void particle::checkBoundary(const double upperBoundary, const double lowerBoundary){
    if(_normalizedPos > upperBoundary){
        _normalizedPos -= (upperBoundary - lowerBoundary);
    } 
    else if(_normalizedPos < lowerBoundary){
        _normalizedPos += (upperBoundary - lowerBoundary);
    } 
}

particle::~particle(){
    _totalNumPtl--;
}