clc;
clf;
clear;
close;
%% DATA
fileName = '../OUTPUT/OUTPUT.h5';
data = loadOutput(fileName);
%% Check and make folders
folderName = mkSaveDir();
%% DATA POST-PROCESSING
% KE[t] = 1/2 * m * v[t-Δt/2] * v[t+Δt/2]
data.particle.KE = zeros(data.NUMBER_OF_STEPS-1, data.NUMBER_OF_PARTICLES);
for i = 1:data.NUMBER_OF_PARTICLES
    data.particle.KE(1,i)=0.5*data.particle.vel(1,i)^2; % [Normalized] 1/2 m/m_e (v/λω)^2
end
for i = 2:(data.NUMBER_OF_STEPS-1)
    for j = 1:data.NUMBER_OF_PARTICLES
        data.particle.KE(i,j)=0.5*data.particle.vel(i,j)*data.particle.vel(i+1,j);
    end
end
% total energy
ESE = zeros(data.NUMBER_OF_STEPS-1, 1);
KE  = zeros(data.NUMBER_OF_STEPS-1, 1);
TE  = zeros(data.NUMBER_OF_STEPS-1, 1);
for i = 1:(data.NUMBER_OF_STEPS-1)
    ESE(i) = sum(data.grid.ESE(i, 1:data.NUMBER_OF_GRIDS)); 
    KE(i)  = sum(data.particle.KE(i, :));
    TE(i)  = ESE(i)+KE(i);
end
% particle velocity alignment
for i = 2:(data.NUMBER_OF_STEPS-1)
    data.particle.vel(i, :) = 0.5*(data.particle.vel(i, :) + data.particle.vel(i+1,:)); 
end
data.particle.vel = data.particle.vel(1:(data.NUMBER_OF_STEPS-1), :);
% particle speed(magnitude of velocity)
for i = 1:(data.NUMBER_OF_STEPS-1)
    data.particle.speed(i, :) = abs(data.particle.vel(i,:));
end
%matrix potential solution for the first step
phi_m = zeros(data.NUMBER_OF_NODES, 1);
sum0 = 0;
for i = 1:data.NUMBER_OF_GRIDS
    sum0 = sum0 - i*(data.DELTA_X^2)*data.grid.rho(1,i);
end
phi_m(1) = sum0/data.NUMBER_OF_GRIDS;
phi_m(data.NUMBER_OF_NODES) = phi_m(1);
phi_m(2) = -(data.DELTA_X^2)*data.grid.rho(1,1) + phi_m(1)*2;
for i = 2:(data.NUMBER_OF_GRIDS-2)
    phi_m(i+1) = 2*phi_m(i) - phi_m(i-1) - (data.DELTA_X^2)*data.grid.rho(1,i);
end
% speed distribution function
numbins = 100;
maxVel = max(max(abs(data.particle.vel)));
lenVel = size(data.particle.vel, 1);
data.VEL_DOMAIN = linspace(0, maxVel, numbins);
delVel = data.VEL_DOMAIN(2) - data.VEL_DOMAIN(1);
data.VEL_DOMAIN = data.VEL_DOMAIN + delVel*0.5*ones(1,numbins);
data.particle.speedDist = zeros(lenVel,numbins);
for i = 1:lenVel
    for j = 1:data.NUMBER_OF_PARTICLES
        index = floor(data.particle.speed(i,j)/delVel)+1;
        data.particle.speedDist(i,index) = data.particle.speedDist(i,index)+1;
    end
end
clearvars i j numbins maxVel delVel 
%% FIGURE
figure_handle = figure(1);
figure_handle.Units = 'normalized';
figure_handle.OuterPosition=[0,0,1,1];
%% PLOT
timeIndex=1;
subplot(2,2,1);
timeEnergyPlot(KE, ESE, TE, data, timeIndex);
subplot(2,2,2);
phaseSpacePlot(data, timeIndex);
subplot(2,2,3);
nodePhiEfieldPlot(data, timeIndex);
subplot(2,2,4);
nodeRhoPlot(data, timeIndex);
%% SAVE PLOT
fileName = strcat(folderName, '/','t-E t-X plot.png');
saveas(figure_handle, fileName);
%% FULL VIDEO
close all;
clearvars figure_handle
figure_handle = figure(1);
figure_handle.Units = 'normalized';
figure_handle.OuterPosition=[0,0,1,1];
fileName = strcat(folderName, '/', 'animation.avi');
v = VideoWriter(fileName);
open(v);
textprogressbar('POST-PROCESSING: ');
for i = 1:lenVel
    clf;
    timeIndex=i;
    subplot(2,2,1);
    timeEnergyPlot(KE, ESE, TE, data, timeIndex);
    subplot(2,2,2);
    phaseSpacePlot(data, timeIndex);
    subplot(2,2,3);
    nodePhiEfieldPlot(data, timeIndex);
    subplot(2,2,4);
    nodeRhoPlot(data, timeIndex);
    I=getframe(gcf);
    writeVideo(v,I);
    textprogressbar(i/lenVel*100);
end
close(v);
textprogressbar('done');
clearvars i timeIndex I v fileName
%% USER DEFINED FUNCTIONS
function [data] = loadOutput(filename)
    data.NUMBER_OF_PARTICLES = double(h5read(filename, '/__NUMBER_OF_PARTICLES__'));
    data.NUMBER_OF_STEPS     = double(h5read(filename, '/__NUMBER_OF_STEPS__'));
    data.NUMBER_OF_NODES     = double(h5read(filename, '/__NUMBER_OF_NODES__'));
    data.NUMBER_OF_GRIDS     = double(h5read(filename, '/__NUMBER_OF_GRIDS__'));
    data.DELTA_X             = h5read(filename, '/__DELTA_X__');
    data.DELTA_T             = h5read(filename, '/__DELTA_T__');
    data.NODE_DOMAIN         = h5read(filename, '/NODE_DOMAIN');
    data.TIME_DOMAIN         = h5read(filename, '/TIME_DOMAIN');
    data.ELECTRON_MASS       = h5read(filename, '/__ELECTRON_MASS__');
    data.ELEMENT_CHARGE      = h5read(filename, '/__ELEMENT_CHARGE__');
    data.EPSILON_ZERO        = h5read(filename, '/__EPSILON_ZERO__');
    data.H2_MASS             = h5read(filename, '/__H2_MASS__');
    data.DEFAULT_WEIGHT      = h5read(filename, '/__DEFAULT_WEIGHT__');
    data.omega_p             = h5read(filename, '/plasma frequency');
    data.particle.pos        = h5read(filename, '/particle data/normalized particle position')';
    data.particle.vel        = h5read(filename, '/particle data/normalized particle velocity')';
    data.grid.rho            = h5read(filename, '/grid data/normalized node charge density')';
    data.grid.phi            = h5read(filename, '/grid data/normalized node potential')';
    data.grid.Efield         = h5read(filename, '/grid data/normalized node electric field')';
    data.grid.ESE            = h5read(filename, '/grid data/normalized node electrostatic field energy')';
end

function folderName = mkSaveDir()
    folderName = '../POST-PROCESSING';
    t = datetime('now');
    folderName = strcat(folderName, '/', 'PLOT', datestr(t,' dd-mmm-yyyy HH MM'));
    mkdir(folderName);
    fileName = strcat(folderName, '/', 'log-', datestr(t, 'dd-mmm-yyyy HH MM'));
    diary(fileName);
    current_dir = pwd;
    cd(folderName);
    save_dir = pwd;
    fprintf("Save root: %s\n", save_dir);
    cd(current_dir);
    clearvars current_dir t fileName
end

function timeEnergyPlot(KE, ESE, TE, data, timeIndex)
    hold on;
    colororder('default')
    yyaxis left
    plot1_ = plot(data.TIME_DOMAIN(1:end-1), KE, '-',  'Color', [0.0000 0.4470 0.7410], 'LineWidth', 1.5, 'DisplayName', '$KE = \frac{1}{2}\frac{m}{m_{e}}(\frac{v}{\lambda_{D}\omega_{p}})^{2}$'); 
    plot2_ = plot(data.TIME_DOMAIN(1:end-1), TE, '-.', 'Color', [0.0000 0.4470 0.7410], 'LineWidth', 1.5, 'DisplayName', 'TE = KE+ESE');
    plot(data.TIME_DOMAIN(timeIndex)*[1 1], [0 max(TE)*1.1], '--', 'Color', '#909497', 'LineWidth', 1.0); 
    hold off;
    yyaxis right
    plot3_ = plot(data.TIME_DOMAIN(1:end-1), ESE, '-', 'Color', [0.8500 0.3250 0.0980],'LineWidth', 1.5, 'DisplayName', '$ESE = \frac{1}{2} \sum_i\widetilde{ \rho }(i)\widetilde{ \phi }(i)$');
    data.plot_ = [plot1_; plot2_; plot3_];
    data.plotlist_ = {plot1_.DisplayName; plot2_.DisplayName; plot3_.DisplayName};
    setTimeEnergyPlot(KE, ESE, data);
end

function setTimeEnergyPlot(KE, ESE, data)
    xlabel('Normalized Time, t=t\omega_{p}');
    yyaxis left
    ylabel('Normalized Energy');
    xlim([data.TIME_DOMAIN(1), data.TIME_DOMAIN(data.NUMBER_OF_STEPS-1)]);
    ylim([0, max(KE)*1.1]);
    yyaxis right
    ylim([0, max(ESE)*1.1]);
    ax=gca; 
    ax.FontSize=10;
    ax.LineWidth=1.0;
    ax.LabelFontSizeMultiplier=1.8;
    ax.FontWeight='bold';
    ax.TickDir='in';
    grid on;
    box on;
    legend(data.plot_, data.plotlist_, 'Interpreter','latex');
    rmfield(data, {'plot_','plotlist_'});
end

function timePosPlot(data)
    hold on;
    plot(data.TIME_DOMAIN, data.particle.pos(:,1*floor(data.NUMBER_OF_PARTICLES/4.0)), 'k-',  'LineWidth', 1.5);
    plot(data.TIME_DOMAIN, data.particle.pos(:,2*floor(data.NUMBER_OF_PARTICLES/4.0)), 'r--', 'LineWidth', 1.5);
    plot(data.TIME_DOMAIN, data.particle.pos(:,3*floor(data.NUMBER_OF_PARTICLES/4.0)), 'b:',  'LineWidth', 1.5);
    hold off;
    setTimePosPlot(data);
end

function setTimePosPlot(data)
    xlabel('Normalized Time, t=t\omega_{p} [.]');
    ylabel('Normalized Position, x=x/\lambda_{D} [.]');
    xlim([data.TIME_DOMAIN(1), data.TIME_DOMAIN(data.NUMBER_OF_STEPS)]);
    ylim([data.NODE_DOMAIN(1), data.NODE_DOMAIN(data.NUMBER_OF_NODES)]);
    ax=gca; 
    ax.FontSize=16;
    ax.LineWidth=1.0;
    ax.LabelFontSizeMultiplier=1.8;
    ax.TitleFontWeight='bold';
    ax.FontWeight='bold';
    ax.TickDir='in';
    grid on;
    box on;
end

function phaseSpacePlot(data, timeIndex)
    size=10;
    scatter(data.particle.pos(timeIndex,:), data.particle.vel(timeIndex,:), size, data.particle.speed(timeIndex,:), 'filled');
    setPhaseSpacePlot(data);
end

function setPhaseSpacePlot(data)
    caxis([0 max(max(data.particle.speed))]);
    xlabel('Normalized Position, x=x/\lambda_{D}');
    ylabel('Normalized Velocity, v=v/\lambda_{D}/\omega_{p}');
    xlim([min(data.NODE_DOMAIN), max(data.NODE_DOMAIN)]);
    ylim([min(min(data.particle.vel)), max(max(data.particle.vel))]);
    ax=gca;
    ax.FontSize=10;
    ax.LineWidth=1.0;
    ax.LabelFontSizeMultiplier=1.8;
    ax.FontWeight='bold';
    ax.TickDir='in';
    grid on;
    box on;
end

function nodePhiEfieldPlot(data, timeIndex)
    colororder('default');
    yyaxis left
    plot(data.NODE_DOMAIN, data.grid.Efield(timeIndex,:), '-', 'Color', [0.0000 0.4470 0.7410], 'LineWidth', 1.5, 'DisplayName', 'E field');
    yyaxis right
    plot(data.NODE_DOMAIN, data.grid.phi(timeIndex,:), '-', 'Color', [0.8500 0.3250 0.0980], 'LineWidth', 1.5, 'DisplayName', 'Potential');
    setNodePhiEfieldPlot(data);
end

function setNodePhiEfieldPlot(data)
    xlabel('Normalized Position, x=x/\lambda_{D}');
    xlim([data.NODE_DOMAIN(1), data.NODE_DOMAIN(data.NUMBER_OF_NODES)]);
    yyaxis left
    ylabel('Normalized E field E=E|e|/m_{e}\lambda_{D}\omega_{p}^{2}');
    ylim([min(min(data.grid.Efield))*1.1, max(max(data.grid.Efield))*1.1]);
    yyaxis right
    ylabel('Normalzied \Phi=\Phi|e|//m_{e}\lambda_{D}^{2}\omega_{p}^{2}');
    ylim([min(min(data.grid.phi))*1.1, max(max(data.grid.phi))*1.1]);
    ax=gca;
    ax.FontSize=10;
    ax.LineWidth=1.0;
    ax.LabelFontSizeMultiplier=1.8;
    ax.FontWeight='bold';
    ax.TickDir='in';
    grid on;
    box on;
    legend();
end

function nodeRhoPlot(data, timeIndex)
    plot(data.NODE_DOMAIN, data.grid.rho(timeIndex,:), '-', 'Color', [0.8500 0.3250 0.0980], 'LineWidth', 1.5, 'DisplayName', 'Charge density');
    setNodeRhoPlot(data);
end

function setNodeRhoPlot(data)
    xlabel('Normalized Position, x=x/\lambda_{D}');
    xlim([data.NODE_DOMAIN(1), data.NODE_DOMAIN(data.NUMBER_OF_NODES)]);
    ylabel('Normalized \rho=\rho/|e|n_{0}');
    ylim([min(min(data.grid.rho))*1.1, max(max(data.grid.rho))*1.1]);
    ax=gca;
    ax.FontSize=10;
    ax.LineWidth=1.0;
    ax.LabelFontSizeMultiplier=1.8;
    ax.FontWeight='bold';
    ax.TickDir='in';
    grid on;
    box on;
end

function speedDistPlot(data, timeIndex)
    maxVel = max(max(data.particle.speed));
    data.speedHistogram = histogram(data.particle.speed(timeIndex,:), linspace(0, maxVel, 100));
    setSpeedDistPlot(data);
end

function setSpeedDistPlot(data)
    xlabel('Normalized Velocity, v=v/\lambda_{D}/\omega_{p}');
    ylabel('# of Super-particles');
    ylim([0 max(max(data.particle.speedDist))*0.5]);
    ax=gca;
    ax.FontSize=10;
    ax.LineWidth=1.0;
    ax.LabelFontSizeMultiplier=1.8;
    ax.FontWeight='bold';
    ax.TickDir='in';
    grid on;
    box on;
end
