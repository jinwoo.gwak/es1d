#include "define.h"
#include "particle.h"

class grid{
private:
    /**
     * @brief Grid class
     * 
     * Note, basically, following variables are 1D double-type arrays with same length.
     * And all of the variables have elements in the same ascending order.
     * ( i.e. position and charge density of the i-th node is _normalizedPos[i] and _normalizedRho[i] respectively. )
     * 
     * _normalizedPos
     * _normalizedRho
     * _normalizedPhi
     * _normalizedEfield
     * 
     * And these variables are 'periodic' except '_normalizedPos'
     * Which means the last element is identical with the first element.
     * 
     * 
     * _numNode               number of nodes
     * 
     * _numGrid               number of grids, note, this value is always (_numNode) - 1
     * 
     * _deltaX                distance between adjacent two nodes. It is also normalized by the debye length.
     *                        ( i.e. _deltaX = 1.0 means that the each nodes are spaced one debye length apart.)
     * 
     * _domainSize            the normalized length of the simulation domain
     *                        ( i.e. _domainSize = 6.28... means that the length of the computational box is 2π )
     * 
     * _lowerLimit            the left limit of the node positions 
     * 
     * _upperLimit            the right limit of the node positions
     * 
     * _normalizedPos         normalized node positions at each nodes 
     *                        ( i.e. _noramlizedPos[i] = __DELTA_X__*i )
     *     
     * _normalizedRho         normalized charge densities at each nodes
     *                        ( i.e. _normalizedRho[i] = 1 - n_e[i]/n_0 )
     *     
     * _normalizedPhi         normalized electro-static potentials at each nodes
     *                        ( i.e. _normalizedPhi[i] = |e|*Phi[i]/(m_e*lambda*omega_p^2) )
     *     
     * _normalizedEfield      normalized electric fields at each nodes 
     *                        ( i.e. _normalizedEfield[i] = |e|*E[i]/(m_e*lambda*omega_p^2) )
     * 
     * _fftInput              input array of fourier transform
     * 
     * _fftOutput             output array of fourier transform
     * 
     * _fftPlan               fourier transform plan variable
     *                        ( to use fftw3 library, this one is needed )
     * 
     * _K                     frequency variable
     */

    const int _numNode;
    const int _numGrid;
    const double _deltaX;
    const double _domainSize;
    const double _lowerLimit;
    const double _upperLimit;
    double *_normalizedPos;
    double *_normalizedRho;
    double *_normalizedPhi;
    double *_normalizedEfield;
    fftw_complex *_fftInput;
    fftw_complex *_fftOutput;
    fftw_plan _fftPlan;
    double *_K;
public:
    /**
     * @brief Construct a new grid object, default constructor
     * 
     */
    grid();

    /**
     * @brief Construct a new grid object
     * 
     * @param numNode         number of nodes ( note, not number of grids )
     * @param deltaX          distance between adjacent two nodes
     */
    grid(const int numNode, const double deltaX);

    /**
     * @brief Construct a new grid object
     * 
     * @param numNode         number of nodes ( note, not number of grids )
     * @param deltaX          distance between adjacent two nodes 
     * @param X0              initial node position, ( i.e. _normalizedNodePos[0] = X0 )
     */
    grid(const int numNode, const double deltaX, const double X0);

    /**
     * @brief Get the number of nodes
     * 
     * It sames with the length or many member variables, namely, 
     * _nodrmalizedNodePos, _normalizedNodeRho, _normalizedNodePhi, _normalizedNodeEfield
     * 
     * @return number of nodes 
     */
    int getNumNode() const;

    /**
     * @brief Get the number of grids
     * 
     * It sames with the length of following member variables, namely,
     * _fftInput, _fftOutput, _fftPlan
     * 
     * @return number of grids
     */
    int getNumGrid() const;

    /**
     * @brief Get the distance between adjacent two nodes 
     * 
     * Note, this value is normalized by the debye length of the simulation which is determined 
     * by the initial distribution function of the particles.
     * 
     * @return the distance between adjacent two nodes 
     */
    double getDeltaX() const;

    /**
     * @brief Get the Domain Size object
     * 
     * @return domain size of the grid object in normalized unit [L=L/lambda]
     */
    double getDomainSize() const;

    /**
     * @brief Get the Lower Limit of the node positions 
     * 
     * @return lower limit of the node positions of the grid object in normalized unit
     */
    double getLowerLimit() const;

    /**
     * @brief Get the Upper Limit of the node positions 
     * 
     * @return upper limit of the node positions of the grid object in normalized unit 
     */
    double getUpperLimit() const;

    /**
     * @brief Get the normalized position of the node
     * 
     * @param index           the index of the node
     * @return normalzied position of the node
     */
    double getNormalizedPos(const int index) const;

    /**
     * @brief Get the normalized charge density of the node
     * 
     * this 'normalized' charge density means, rho = 1 - n_e/n_0 
     * when n_e is electron number density and n_0 is background neutralizing hydrogen ion(H+) number density
     * @param index           the index of the node
     * @return normalized charge density of the node 
     */
    double getNormalizedRho(const int index) const;

    /**
     * @brief Get the normalized electrostatic potential of the node
     * 
     * @param index           the index of the node
     * @return normalized electrostatic potential of the node
     */
    double getNormalizedPhi(const int index) const;

    /**
     * @brief Get the normalized electric field of the node
     * 
     * @param index           the index of the node
     * @return normalized electric field of the node 
     */
    double getNormalizedEfield(const int index) const;

    /**
     * @brief Print the normalized charge density at each nodes 
     * 
     */
    void showNormalizedRho() const;

    /**
     * @brief Print the normalized electric field at each nodes 
     * 
     */
    void showNormalizedEfield() const;

    /**
     * @brief Print the normalized electrostatic potential at each nodes 
     * 
     */
    void showNormalizedPhi() const;

    
    /**
     * @brief Accumulate the normalized charge density at the node 
     * 
     * @param index           the index of the node
     * @param chargeDensity   normalized charge density 
     */
    void addNormalizedRho(const int index, const double chargeDensity);

    /**
     * @brief Accumulate the normalized charge densities from the particles
     * 
     * @param particles       address of the first element of the array of pointers to point the particle objects
     */
    void addNormalizedRho(particle** particles);

    /**
     * @brief Accumulate the normalized charge densities to the edge nodes. 
     * It gives a periodic boundary condition.
     */
    void addPeriodicRho();

    /**
     * @brief Accumulate the given normalized charge density to the all of nodes equally.
     * 
     * @param backGroundRho   the charge density
     */
    void addBackgroundRho(const double backGroundRho);

    /**
     * @brief Set all of the _normalizedRho[i] elements as zero
     * 
     */
    void clearNormalizedRho();

    /**
     * @brief Set all of the _normalizedPhi[i] elements as zero
     * 
     */
    void clearNormalizedPhi();

    /**
     * @brief Set all of the _normalizedEfield[i] elements as zero
     * 
     */
    void clearNormalizedEfield();
    
    /**
     * @brief solve the poisson's eqn and update the _normalizedEfield[i]
     * 
     */
    void solvePoisson();

    /**
     * @brief Destroy the grid object
     * 
     */
    ~grid();                          
};