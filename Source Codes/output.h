#include "define.h"
#include "grid.h"

class output{
private:
    // file identifier
    const hid_t fileId, ptlGroup, gridGroup;

    // data space identifier
    hid_t dataSpace0D;
    hid_t dataSpace1D_time, dataSpace1D_node;
    hid_t dataSpace2D_ptl,  dataSpace2D_node;

    // memory space identifier
    hid_t memorySpace2D_node, memorySpace2D_ptl;

    // arbitrary dataSet identifier
    hid_t dataSet, ptlPosDataSet, ptlVelDataSet;
    hid_t nodeEnergyDataSet, nodeEfieldDataSet, nodeRhoDataSet, nodePhiDataSet;

    // arbitrary attribute identifier
    hid_t attribute, attributeType;

    // dataSet dimension variables, necessary for data space variables
    hsize_t dataSetDim1D[1];
    hsize_t dataSetDim2D[2];
    hsize_t offset[2];
    hsize_t count[2];
    hsize_t stride[2];
    hsize_t block[2];

    // dummy variables
    int    _time_index;
    int    _val_int;
    double _val_double;
    double _val_time_domain[__NUMBER_OF_STEPS__];
    double _val_node_domain[__NUMBER_OF_NODES__];
    double _val_ptl_domain[__NUMBER_OF_PARTICLES__];

public:
    // constructor
    output();

    // destructor
    ~output();

    void updateTimeIndex();
    void writePtlPosition(particle **particles);
    void writePtlVelocity(particle **particles);
    void writeNodeEnergy(grid &grid);
    void writeNodeEfield(grid &grid);
    void writeNodeRho(grid &grid);
    void writeNodePhi(grid &grid);
};